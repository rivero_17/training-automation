package com.sodimac.config;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ejerciciosodimac {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.get("https://www.sodimac.cl/");
		driver.manage().window().maximize();
		WebElement search= driver.findElement(By.id("searchBoxInput"));
		search.sendKeys("silla gris");
		search.sendKeys(Keys.ENTER);
		driver.findElement(By.id("testId-Dropdown-dd-sort-options-value")).click();
		driver.findElement(By.xpath("//*[@id=\"testId-li-testId-DropdownList-testId-Dropdown-dd-sort-options-dropdown-list-item-4\"]")).click();
		// buscando el primer producto
		WebElement element = driver.findElement(By.id("testId-Link-img-pdp-link"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
		//seleccionando el nombre del producto
		Thread.sleep(5000);
		System.out.println(driver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div[4]/div[2]/div[1]/div[1]/h1")).getText());
		//seleccionando el sku
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\\\"__next\\\"]/div/div/div[4]/div[2]/div[1]/div[1]/div[2]/div[2]")));
		WebElement sku= driver.findElement(By.xpath("\"//*[@id=\\\\\\\"__next\\\\\\\"]/div/div/div[4]/div[2]/div[1]/div[1]/div[2]/div[2]\""));
		System.out.println(sku.getText());
		
		//driver.close();
				
		
		
		
	

	}

}
