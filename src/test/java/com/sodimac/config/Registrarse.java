package com.sodimac.config;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Registrarse {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.get("https://www.yapo.cl/");
		driver.manage().window().maximize();
		//clase para registrarse 
		driver.findElement(By.id("login-account-link")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/form/p[2]/strong/a")).click();
		
		//llenando el formulario
		driver.findElement(By.id("account_name")).sendKeys("Rolando Rivero");//Nombre completo
		driver.findElement(By.id("phone")).sendKeys("987321829");//Telefono
		driver.findElement(By.id("account_email")).sendKeys("rrivero1@clmconsultores.com");//correo
		driver.findElement(By.id("account_password")).sendKeys("12345678");//password
		driver.findElement(By.id("account_password_verify")).sendKeys("12345678");
		WebElement persona = driver.findElement(By.cssSelector("#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div"));
	    persona.click();
	    WebElement genero=driver.findElement(By.cssSelector("div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper"));
	    genero.click();
	    Select region=new Select (driver.findElement(By.id("account_region")));
	    region.selectByVisibleText("Región Metropolitana");
	    WebElement comuna=driver.findElement(By.id("account_commune"));
	    comuna.sendKeys("Huechuraba");
	    WebElement terminos=driver.findElement(By.id("accept_conditions"));
		Actions actions = new Actions(driver);
		actions.moveToElement(terminos).click().perform();
		//driver.findElement(By.id("edit_profile_btn")).click();// boton crear cuenta
		
		//buscando avisos
		
		driver.findElement(By.xpath("//a[contains(text(),'Buscar Avisos')]")).click();
		Thread.sleep(15000);
		//WebDriverWait wait = new WebDriverWait(driver, 15000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("searchtext")));
		driver.findElement(By.id("searchtext")).sendKeys("casa");
		Select filtro_region= new Select(driver.findElement(By.id("searcharea_expanded")));
		filtro_region.selectByVisibleText("V Valparaíso");
		driver.findElement(By.xpath("//*[@id=\"communes\"]")).click();
		Thread.sleep(5000);
		WebElement comuna2=driver.findElement(By.xpath("//*[@id=\"multicom-widget\"]/label[2]/div"));
		Actions a2 = new Actions(driver);
		a2.moveToElement(comuna2).click().perform();
		driver.findElement(By.id("catgroup")).click();
		driver.findElement(By.xpath("//*[@id=\"catgroup\"]/option[4]")).click();	
		Select tipo_casa= new Select(driver.findElement(By.id("estate_type_ret")));
		tipo_casa.selectByVisibleText("Casa");
		Select price_min= new Select(driver.findElement(By.id("ps_2")));
		price_min.selectByVisibleText("$ 300.000");
		Select price_max= new Select(driver.findElement(By.id("pe_2")));
		price_max.selectByVisibleText("$ 600.000");
		Select dor_min= new Select(driver.findElement(By.id("rooms_ros")));
		dor_min.selectByVisibleText("2 dormitorios");
		Select dor_max= new Select(driver.findElement(By.id("rooms_roe")));
		dor_max.selectByVisibleText("3 dormitorios");
		Select m2_min= new Select(driver.findElement(By.id("ss_0")));
		m2_min.selectByVisibleText("60 m²");
		Select m2_max= new Select(driver.findElement(By.id("se_0")));
		m2_max.selectByVisibleText("180 m²");
		Select bano_min= new Select(driver.findElement(By.id("bathrooms_brs")));
		bano_min.selectByVisibleText("1 baño");
		Select bano_max= new Select(driver.findElement(By.id("bathrooms_bre")));
		bano_max.selectByVisibleText("2 baños");
		Select gc_min= new Select(driver.findElement(By.id("condominio_cos")));
		gc_min.selectByVisibleText("$ 25.000");
		Select gc_max= new Select(driver.findElement(By.id("condominio_coe")));
		gc_max.selectByVisibleText("$ 150.000");
		Select garaje= new Select(driver.findElement(By.id("garage_spaces_gs")));
		garaje.selectByVisibleText("1");
		driver.findElement(By.id("searchbutton")).click();
		
		//publicar aviso
		
		driver.findElement(By.xpath("//*[@id=\"region_tab\"]/div[1]/div[5]/div[3]/ul/li[7]/a/span")).click();
		WebDriverWait espera = new WebDriverWait(driver, 5000);
		espera.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"category_group\"]")));
		WebElement category=driver.findElement(By.xpath("//*[@id=\"category_group\"]"));
		category.click();
		Select list= new Select (driver.findElement(By.xpath("//*[@id=\"category_group\"]")));
		list.selectByVisibleText("Moda y vestuario");
		Thread.sleep(1000);
		Select condicion= new Select(driver.findElement(By.id("condition")));
		condicion.selectByVisibleText("Nuevo");
		Thread.sleep(1000);
		Select gender= new Select(driver.findElement(By.id("gender")));
		gender.selectByVisibleText("Femenino");
		Thread.sleep(1000);
		Select talla= new Select(driver.findElement(By.id("clothing_size")));
		talla.selectByVisibleText("S");	
		driver.findElement(By.xpath("//*[@id=\"subject\"]")).sendKeys("Vestido blanco");
		driver.findElement(By.id("body")).sendKeys("Vestido nuevo marca Calvin Klein Talla S");
		driver.findElement(By.id("price")).sendKeys("$ 60.000");
		WebElement fotos= driver.findElement(By.xpath("//*[@id=\"image_upload_button\"]/input"));
		fotos.sendKeys("C:\\Users\\Rolando Rivero\\Desktop\\800mil.jpg");	
		driver.findElement(By.id("submit_create_now")).click();
		   
		
		
	}

}
