package com.sodimac.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sodimac.config.BaseConfigSodimac;
import com.yapo.config.BaseConfig;
import com.yapo.util.WaitWebDriver;

public class HomeSodimac extends BaseConfigSodimac {	
	public HomeSodimac(WebDriver driver) {
		 this.driver = driver;
		    PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "/html/body/header/div/section[2]/div[2]/section[3]/div[2]/div/div[1]/input")
	private WebElement InputBox;	
	
	@FindBy (xpath = "//*[@id=\"fbra_searchBoxContainer\"]/div/div[1]/div")
	private WebElement Search;
	
	@FindBy (xpath = "//*[@id=\"testId-Dropdown-dd-sort-options-value\"]")
	private WebElement Options;
	
	@FindBy(xpath = "/html/body/div/div/div/div[2]/div[3]/div[2]/div[1]/div[3]/div/div/div[1]/div[1]/div/div/ul/li[5]")
	private WebElement Listprice;
	
	
	@FindBy(xpath = "/html/body/div/div/div/div[3]/div[3]/div[2]/div[1]/div[4]/div[1]/div/div[3]/a/div")
	private  String Productname;
	
	

	
public boolean clickPublicarAviso() {
	try {	
		
		Actions action = new Actions(driver);
		action.moveToElement(InputBox).build().perform();
		Actions seriesOfActions = action.moveToElement(InputBox).click().sendKeys(InputBox, "Silla");
		seriesOfActions.perform();
		Search.click();
		
		Actions option = new Actions(driver);
		option.moveToElement(Options).build().perform();
		Actions seriesOfOptions = option.moveToElement(Options).click();
		seriesOfOptions.perform();
		
		Actions list = new Actions(driver);
		list.moveToElement(Listprice).build().perform();
		Actions seriesOfList = list.moveToElement(Listprice).click();
		seriesOfList.perform();
		
		
		WebElement TxtBoxContent = driver.findElement(By.id(Productname));
		TxtBoxContent.getText();
		System.out.println("Printing " + TxtBoxContent.getAttribute("value"));
		
	
		
		
		/*System.out.println("Ingresando a B");
		Actions action = new Actions(driver);
		System.out.println(String.format("Haciendo click en %s", InputBox.getText()));
		action.moveToElement(InputBox).build().perform();
		InputBox.click();
		action.sendKeys("Silla Gamer");
		action.moveToElement(Search);
		Search.click();*/
	//		
		
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}
}
}
