package com.project.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	
	private WebDriver driver;
	
	public Base (WebDriver driver) {
		this.driver= driver;
	}
	
	public WebDriver chromeDriverConnection() {
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		return driver;
	}
	
	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}
	
	public void type (String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
		
	}
	
	public void click (By locator) {
		driver.findElement(locator).click();
		
	}
	
	public void visit(String url) {
		driver.get(url);		
	}
	



}
