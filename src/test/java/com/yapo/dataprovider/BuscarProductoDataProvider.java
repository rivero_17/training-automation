package com.yapo.dataprovider;
import org.testng.annotations.Test;
import com.google.gson.Gson;
// import com.yapo.entities.DatosBuscar;
// import com.yapo.entities.DatosFormulario;
// import com.yapo.entities.DatosPublicar;
// import com.yapo.entities.DatosRegistro;
import com.yapo.entities.DatosFalabella;
import org.testng.annotations.DataProvider;

public class BuscarProductoDataProvider {

	@DataProvider 
	public static Object[][] buscarProducto() {
		Gson gson = new Gson();
		DatosFalabella datosFalabella = gson.fromJson(FileDataProvider.asString(String.format("./resource/Busqueda.json")),
				DatosFalabella.class);

		return new Object[][] { { datosFalabella } };
    };
    

}