package com.yapo.dataprovider;

import org.testng.annotations.Test;
import com.google.gson.Gson;
import com.yapo.entities.DatosBuscar;
import com.yapo.entities.DatosFormulario;
import com.yapo.entities.DatosPublicar;
import com.yapo.entities.DatosRegistro;

import org.testng.annotations.DataProvider;

public class RegisterDataProvider {

	@DataProvider 
	public static Object[][] registro() {
		Gson gson = new Gson();
		DatosRegistro datosRegistro = gson.fromJson(FileDataProvider.asString(String.format("./resource/JsonRegister.json")),
				DatosRegistro.class);

		return new Object[][] { { datosRegistro } };
    };
    
	@DataProvider
	public static Object[][] publicar() {
		Gson gson = new Gson();
		DatosPublicar datosPublicar = gson.fromJson(FileDataProvider.asString(String.format("./resource/JsonPublicar.json")),
				DatosPublicar.class);

		return new Object[][] { { datosPublicar } };
	};
	
	@DataProvider
	public static Object[][] buscar() {
		Gson gson = new Gson();
		DatosBuscar datosBuscar = gson.fromJson(FileDataProvider.asString(String.format("./resource/JsonBuscar.json")),
				DatosBuscar.class);

		return new Object[][] { { datosBuscar } };
	}
  }





