package com.yapo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.yapo.config.BaseConfig;
import com.yapo.util.WaitWebDriver;

public class RegisterPage extends BaseConfig {
	
	public RegisterPage(WebDriver driver) {
		//super();
		this.driver=driver;
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
		
	}
	@FindBy (id="login-account-link")
	private WebElement inicioSesion;
	@FindBy (xpath=("/html/body/div[9]/div/div/div[2]/form/p[2]/strong/a"))
	private WebElement crearCuentaLink;
	@FindBy (id="account_name")
	private WebElement nombreCompleto;
	@FindBy (id="phone")
	private WebElement telefono;
	@FindBy (id="account_email")
	private WebElement correo;
	@FindBy (id="account_password")
	private WebElement password;
	@FindBy (id="account_password_verify")
	private WebElement confirmarPassword;
	@FindBy (css="#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div")
	private WebElement persona;
	@FindBy (css="div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper")
	private WebElement genero;
	
	@FindBy (id="account_region")
	private WebElement regiones;
	
	@FindBy (id="account_commune")
	private WebElement comunas;
	@FindBy (id="accept_conditions")
	private WebElement terminos;
	@FindBy (id="edit_profile_btn")
	private WebElement crear_cuenta_boton;
	
	
	public boolean RegisterUser(String region)  {
							
		try {
			
			
			//driver.findElement(By.id("login-account-link")).click();
			// Thread.sleep(6000);
			driver.navigate().to("https://www2.yapo.cl/cuenta/form/0");
						
			//driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/form/p[2]/strong/a")).click();
			//driver.get("https://www.yapo.cl/region_metropolitana");
			
			//llenando el formulario
			/*driver.findElement(By.id("account_name")).sendKeys("nombre_completo");//Nombre completo
			driver.findElement(By.id("phone")).sendKeys("987321829");//Telefono
			driver.findElement(By.id("account_email")).sendKeys("rrivero1@clmconsultores.com");//correo
			driver.findElement(By.id("account_password")).sendKeys("12345678");//password
			driver.findElement(By.id("account_password_verify")).sendKeys("12345678");
			WebElement persona = driver.findElement(By.cssSelector("#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div"));
		    persona.click();
		    WebElement genero=driver.findElement(By.cssSelector("div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper"));
		    genero.click();*/
			
			WebElement persona = driver.findElement(By.cssSelector("#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div"));
		    persona.click();
		    WebElement genero=driver.findElement(By.cssSelector("div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper"));
		    genero.click();
		    
			WaitWebDriver.waitVisibility(driver, regiones, 10);
			Select selectRegiones = new Select(regiones);
			
			selectRegiones.selectByVisibleText(region);
			
			
			
			
			
			return true;
			
		    //Select region=new Select (driver.findElement(By.id("account_region")));
		    //region.selectByVisibleText("Región Metropolitana");
		    
		    /*WebElement comuna=driver.findElement(By.id("account_commune"));
		    comuna.sendKeys("Huechuraba");
		    WebElement terminos=driver.findElement(By.id("accept_conditions"));
			Actions actions = new Actions(driver);
			actions.moveToElement(terminos).click().perform();
			//driver.findElement(By.id("edit_profile_btn")).click();// boton crear cuenta*/
	
		}
		catch(Exception e){
			e.printStackTrace();
			
			return false;
		}
				
		
	
	}

	public boolean RegisterComuna(String comuna)  {
							
		try {
			System.out.println("alva");
			System.out.println(comuna);
			WaitWebDriver.waitVisibility(driver, comunas, 10);
			Select selectComuna = new Select(comunas);
			selectComuna.selectByVisibleText(comuna);
			return true;

		}
		
		catch(Exception e){
			e.printStackTrace();
			
			return false;
		}
		
	}
	
	public boolean RegisterName(String nombreCompleto)  {
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.nombreCompleto = wait
					.until(ExpectedConditions.elementToBeClickable(this.nombreCompleto));
			//WaitWebDriver.waitVisibility(driver, this.nombreCompleto, 10);
			System.out.println("Ingresando nombre completo");
			this.nombreCompleto.sendKeys(nombreCompleto);
			return true;
		}
		
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	public boolean RegisterTelefono(String telefono)  {
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.telefono = wait
					.until(ExpectedConditions.elementToBeClickable(this.telefono));
			//WaitWebDriver.waitVisibility(driver, this.nombreCompleto, 10);
			System.out.println("Ingresando telefono");
			this.telefono.sendKeys(telefono);
			return true;
		}
		
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	public boolean RegisterPassword(String password)  {
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.password = wait
					.until(ExpectedConditions.elementToBeClickable(this.password));
			System.out.println("Ingresando password");
			this.password.sendKeys(password);
			return true;
			//return password;
		}
		
		catch(Exception e){
			e.printStackTrace();
			return false;
			//return "catch-password-1";
		}
		
	}
	public boolean RegisterCorreo(String correo)  {
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.correo = wait
					.until(ExpectedConditions.elementToBeClickable(this.correo));
			//WaitWebDriver.waitVisibility(driver, this.nombreCompleto, 10);
			System.out.println("Ingresando password");
			this.correo.sendKeys(correo);
			return true;
		}
		
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	public boolean RegisterconfirmarPassword(String password2)  {
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.confirmarPassword = wait
					.until(ExpectedConditions.elementToBeClickable(this.confirmarPassword));
			//WaitWebDriver.waitVisibility(driver, this.nombreCompleto, 10);
			System.out.println("Ingresando password2");
			this.confirmarPassword.sendKeys(password2);
			WebElement terminos=driver.findElement(By.id("accept_conditions"));
			Actions actions = new Actions(driver);
			actions.moveToElement(terminos).click().perform();
			driver.findElement(By.id("edit_profile_btn")).click();// boton crear cuenta*/
			return true;
			//return password2;
		}
		
		catch(Exception e){
			e.printStackTrace();
			return false;
			//return "catch-password-2";
		}
		
	}
}
