package com.yapo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.yapo.config.BaseConfig;
import com.yapo.util.WaitWebDriver;

@Test
public class PublicarAvisoPage extends BaseConfig {
	public PublicarAvisoPage (WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	By clickPublicarAviso= By.xpath("//span[contains(text(),'Publicar aviso')]");

	@FindBy (xpath=("//*[@id=\"category_group\"]"))
	private WebElement categorias;
	@FindBy (id=("condition"))
	private WebElement condicion;
	@FindBy (id=("gender"))
	private WebElement genero;
	@FindBy (id=("clothing_size"))
	private WebElement talla;
	@FindBy (xpath=("//*[@id=\"subject\"]"))
	private WebElement titulo;
	@FindBy (id=("body"))
	private WebElement descripcion;
	@FindBy (id=("price"))
	private WebElement precio;

	@FindBy (id=("region"))
	private WebElement region;
	@FindBy (id=("communes"))
	private WebElement comuna;
	@FindBy (id=("name"))
	private WebElement nombre;
	@FindBy (id=("email"))
	private WebElement email;
	@FindBy (id=("email_confirm"))
	private WebElement emailConfirm;
	@FindBy (id=("phone"))
	private WebElement telefono;
	@FindBy (id=("passwd"))
	private WebElement contrasena;
	@FindBy (id=("passwd_ver"))
	private WebElement confirmaContrasena;

	By imagen=By.xpath("//*[@id=\"image_upload_button\"]/input");
	By botonPublicar= By.id("submit_create_now");
	By radioPersona= By.id("p_ad");
	By aceptarTerminos= By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/form[1]/div[1]/div[1]/ins[1]");



	public boolean PublicarAviso(String categoria) {
	  try {
		  
		  	System.out.println("76");
		  	
		  	WebDriverWait espera = new WebDriverWait(driver, 60);
			espera.until(ExpectedConditions.visibilityOfElementLocated(clickPublicarAviso));
		  	driver.findElement(clickPublicarAviso).click();
		  			  	
		  	WaitWebDriver.waitVisibility(driver, categorias, 10);
			Select selectCategorias = new Select(categorias);
				selectCategorias.selectByVisibleText(categoria);
			
			WebElement fotos= driver.findElement(imagen);
			fotos.sendKeys("C:\\Users\\Rolando Rivero\\Desktop\\800mil.jpg");
			driver.findElement(radioPersona).click();
			driver.findElement(aceptarTerminos).click();
				
			return true;
		  	/*
		  	WebDriverWait espera = new WebDriverWait(driver, 60);
			espera.until(ExpectedConditions.visibilityOfElementLocated(categoria));
			WebElement category=driver.findElement(categoria);
			category.click();
			Select list= new Select (driver.findElement(categoria));
			list.selectByVisibleText("Moda y vestuario");
			Select condicion= new Select(driver.findElement(nuevo));
			condicion.selectByVisibleText("Nuevo");
			Select gender= new Select(driver.findElement(genero));
			gender.selectByVisibleText("Femenino");
			Select listTalla= new Select(driver.findElement(talla));
			listTalla.selectByVisibleText("S");	
			driver.findElement(titulo).sendKeys("Vestido blanco");
			driver.findElement(descripcion).sendKeys("Vestido nuevo marca Calvin Klein Talla S");
			driver.findElement(precio).sendKeys("$ 60.000");
			WebElement fotos= driver.findElement(imagen);
			fotos.sendKeys("C:\\Users\\Rolando Rivero\\Desktop\\800mil.jpg");	
			Select listRegion= new Select (driver.findElement(region));
			listRegion.selectByVisibleText("Región Metropolitana");
			Select listComunas= new Select (driver.findElement(comuna));
			listComunas.selectByVisibleText("Las Condes");
			//Tu información
			driver.findElement(radioPersona).click();
			driver.findElement(nombre).sendKeys("Rolando Rivero");
			driver.findElement(email).sendKeys("rriver@clmconsultores.com");
			driver.findElement(confirmaEmail).sendKeys("rriver@clmconsultores.com");
			driver.findElement(telefono).sendKeys("987321829");
			driver.findElement(contraseña).sendKeys("12345678");
			driver.findElement(confirmaContraseña).sendKeys("12345678");
			driver.findElement(aceptarTerminos).click();
			driver.findElement(botonPublicar).click();*/
		
	  }
	  
	  catch (Exception e){
		e.printStackTrace();
		return false;
	  }
  }
	public boolean PublicarCondicion(String condition) {
		  try {
			  
			  	System.out.println("Seleccionando condicion");
			  	WaitWebDriver.waitVisibility(driver, condicion,60);
				Select selectCondicion = new Select(condicion);
				selectCondicion.selectByVisibleText(condition);	
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarGenero(String gender) {
		  try {
			  
			  	System.out.println("Seleccionando genero");
			  	WaitWebDriver.waitVisibility(driver, genero,60);
				Select selectGenero = new Select(genero);
				selectGenero.selectByVisibleText(gender);	
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarTalla(String medida) {
		  try {
			  
			  	System.out.println("Seleccionando talla");
			  	WaitWebDriver.waitVisibility(driver, talla,60);
				Select selectTalla = new Select(talla);
				selectTalla.selectByVisibleText(medida);	
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarTitulo(String title) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.titulo = wait
						.until(ExpectedConditions.elementToBeClickable(this.titulo));
				System.out.println("Ingresando titulo");
				this.titulo.sendKeys(title);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarDescripcion(String body) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.descripcion = wait
						.until(ExpectedConditions.elementToBeClickable(this.descripcion));
				System.out.println("Ingresando descripcion");
				this.descripcion.sendKeys(body);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarPrecio(String price) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.precio = wait
						.until(ExpectedConditions.elementToBeClickable(this.precio));
				System.out.println("Ingresando precio");
				this.precio.sendKeys(price);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarRegion(String region2) {
		  try {
			  
			  	System.out.println("Seleccionando region");
			  	WaitWebDriver.waitVisibility(driver, region,60);
				Select selectRegion = new Select(region);
				selectRegion.selectByVisibleText(region2);	
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarComuna(String comuna2) {
		  try {
			  
			  	System.out.println("Seleccionando comuna");
			  	WaitWebDriver.waitVisibility(driver, comuna,60);
				Select selectComuna = new Select(comuna);
				selectComuna.selectByVisibleText(comuna2);	
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarNombre(String nombre2) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.nombre = wait
						.until(ExpectedConditions.elementToBeClickable(this.nombre));
				System.out.println("Ingresando nombre");
				this.nombre.sendKeys(nombre2);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarEmail(String email2) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.email = wait
						.until(ExpectedConditions.elementToBeClickable(this.email));
				System.out.println("Ingresando email");
				this.email.sendKeys(email2);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarEmailConfirm(String email3) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.emailConfirm = wait
						.until(ExpectedConditions.elementToBeClickable(this.emailConfirm));
				System.out.println("Ingresando confirmacion de email");
				this.emailConfirm.sendKeys(email3);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarTelefono(String fono) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.telefono = wait
						.until(ExpectedConditions.elementToBeClickable(this.telefono));
				System.out.println("Ingresando telefono");
				this.telefono.sendKeys(fono);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarContrasena(String pas) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.contrasena = wait
						.until(ExpectedConditions.elementToBeClickable(this.contrasena));
				System.out.println("Ingresando contrasena");
				this.contrasena.sendKeys(pas);
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
	public boolean PublicarContrasenaConfirm(String verifyPass) {
		  try {
			  
			  WebDriverWait wait = new WebDriverWait(driver, 60);
				this.confirmaContrasena = wait
						.until(ExpectedConditions.elementToBeClickable(this.confirmaContrasena));
				System.out.println("Confirmando contrasena");
				this.confirmaContrasena.sendKeys(verifyPass);
				//driver.findElement(botonPublicar).click();
				return true;
		  }
		  
		  catch (Exception e){
			e.printStackTrace();
			return false;
		  }
	  }
}
