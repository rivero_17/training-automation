package com.yapo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.yapo.config.BaseConfig;
import com.yapo.util.WaitWebDriver;

@Test
public class BuscarAvisoPage extends BaseConfig {
  public BuscarAvisoPage (WebDriver driver) {
	  this.driver=driver;
  }
  
  	@FindBy (id=("region_name_15"))
	private WebElement categorias;
  	
  	@FindBy (id=("searchtext"))
	private WebElement textBoxLocator;
  	
  	@FindBy (id=("searcharea_expanded"))
	private WebElement filtroRegionLocator;
  	
  	
  	
By regionLocator= By.id("region_name_15");
/*
By filtroRegionLocator= By.id("searcharea_expanded");
By comunaLocator= By.xpath("//*[@id=\"communes\"]");
By comuna2Locator= By.xpath("//*[@id=\"multicom-widget\"]/label[2]/div");
By filtroCategoria= By.id("catgroup");
By categoriaArrendar= By.xpath("//*[@id=\"catgroup\"]/option[4]");
By tipoPropiedad= By.id("estate_type_ret");
By precioMinimo= By.id("ps_2");
By precioMaximo= By.id("pe_2");
By dorMin= By.id("rooms_ros");
By dorMax=By.id("rooms_roe");
By m2Min=By.id("ss_0");
By m2Max=By.id("se_0");
By minBano=By.id("bathrooms_brs");
By maxBano=By.id("bathrooms_bre");
By gcMin=By.id("condominio_cos");
By gcMax=By.id("condominio_coe");
By garaje= By.id("garage_spaces_gs");
By botonBuscar= By.id("searchbutton");*/

	  	  public boolean BuscarAviso(String home) {
		  
		  try {	
			  	driver.findElement(regionLocator).click();
			  	System.out.println("Hola nuevo");
			  
			  /*
			  	WebDriverWait wait = new WebDriverWait(driver, 200);
			  	this.textBoxLocator = wait
			  		.until(ExpectedConditions.elementToBeClickable(this.textBoxLocator));
			  	System.out.println("Ingresando texto a buscar");
				this.textBoxLocator.sendKeys(home);
				*/
				return true;
			  
			  	/*driver.findElement(regionLocator).click();
			  	driver.findElement(textBoxLocator).sendKeys("casa");
				Select filtro_region= new Select(driver.findElement(filtroRegionLocator));
				filtro_region.selectByVisibleText("V Valparaíso");
				driver.findElement(comunaLocator).click();
				//Thread.sleep(5000);
				WebElement comuna2=driver.findElement(comuna2Locator);
				Actions a2 = new Actions(driver);
				a2.moveToElement(comuna2).click().perform();
				driver.findElement(filtroCategoria).click();
				driver.findElement(categoriaArrendar).click();	
				Select tipo_casa= new Select(driver.findElement(tipoPropiedad));
				tipo_casa.selectByVisibleText("Casa");
				Select price_min= new Select(driver.findElement(precioMinimo));
				price_min.selectByVisibleText("$ 300.000");
				Select price_max= new Select(driver.findElement(precioMaximo));
				price_max.selectByVisibleText("$ 600.000");
				Select dor_min= new Select(driver.findElement(dorMin));
				dor_min.selectByVisibleText("2 dormitorios");
				Select dor_max= new Select(driver.findElement(dorMax));
				dor_max.selectByVisibleText("3 dormitorios");
				Select m2_min= new Select(driver.findElement(m2Min));
				m2_min.selectByVisibleText("60 m²");
				Select m2_max= new Select(driver.findElement(m2Max));
				m2_max.selectByVisibleText("180 m²");
				Select bano_min= new Select(driver.findElement(minBano));
				bano_min.selectByVisibleText("1 baño");
				Select bano_max= new Select(driver.findElement(maxBano));
				bano_max.selectByVisibleText("2 baños");
				Select gc_min= new Select(driver.findElement(gcMin));
				gc_min.selectByVisibleText("$ 25.000");
				Select gc_max= new Select(driver.findElement(gcMax));
				gc_max.selectByVisibleText("$ 150.000");
				Select cochera= new Select(driver.findElement(garaje));
				cochera.selectByVisibleText("1");
				driver.findElement(botonBuscar).click();
				System.out.println(driver.findElement(By.xpath("//div[@class='FailoverMessageBox']//table")).getText());
				*/
			  	  
			
		  }
		  catch (Exception e){
				e.printStackTrace();
				return false;
		  }
	  }
	  
	  	public boolean EscogiendoFiltro1(String filtro1) {
	  	  try {
	  		  
	  		  	System.out.println("76");
	  		  	
	   		  	WaitWebDriver.waitVisibility(driver, filtroRegionLocator, 10);
	  			Select selectFiltro1 = new Select(filtroRegionLocator);
	  			selectFiltro1.selectByVisibleText(filtro1);
	  			return true;

	  	  }
	  	  
	  	  catch (Exception e){
	  		e.printStackTrace();
	  		return false;
	  	  }
	    }
 
  }
