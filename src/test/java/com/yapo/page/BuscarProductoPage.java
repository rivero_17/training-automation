package com.yapo.page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yapo.config.BaseConfig;
import com.yapo.util.WaitWebDriver;

public class BuscarProductoPage extends BaseConfig {

	public BuscarProductoPage (WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	final String xpathInput="//div[not(contains(@style,'display:none'))]//input[@type='text'][@placeholder='¿Qué buscas?']";
	@FindBy (xpath=xpathInput)
	private WebElement searchTextBox;
	@FindBy (xpath="//*[@id='buttonForCustomers']/button")
	private WebElement eligeTuopcion;
	@FindBy (xpath="/html/body/div[1]/div/div/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/button")
	private WebElement eligeTalla;
	@FindBy (xpath="//*[@id=\"__next\"]/div/div/div/div/div/div/div[2]/div[2]/div[3]/button[1]")
	private WebElement agregaBolsa;
	
	@FindBy (xpath="/html/body/div[1]/nav/div[3]/div/div[1]/div/div[1]/div/input")
	private WebElement inputBusqueda;
	
	
	@FindBy (xpath="//*[@id='buttonForCustomers']/button")
	private WebElement botonEligeTusOpciones;
	
	@FindBy (xpath="(//*[contains(@class, 'size-options')])[1]//button[1]")
	private WebElement botonPrimeraTalla;
	
	@FindBy (xpath="(//*[contains(@class, 'addToCart-container')])[1]")
	private WebElement botonAgregarABolsa;
	
	@FindBy (xpath="//*[contains(@class, 'addToCart-btn')]//a")
	private WebElement botonIrABolsaDeCompras;
	
	@FindBy (xpath="//*[contains(@class, 'fb-order-status__continue-purchase')]")
	private WebElement botonIrAComprar;
	
	@FindBy (xpath="//select[@id=\"region\"]")
	private WebElement selectRegionesElem;
	
	@FindBy (xpath="//select[@id=\"comuna\"]")
	private WebElement selectComunaElem;
	
	@FindBy (xpath="//form[contains(@class, 'fbra_test_appContainer__form')]//button")
	private WebElement botonContinuar;
	
	@FindBy (id="calle")
	private WebElement inputCalle;
	
	@FindBy (id="streetNumber")
	private WebElement inputNumero;
	
	@FindBy (id="departmentNumber")
	private WebElement inputDepto;
	
	@FindBy (xpath="//*[contains(@class, 'fbra_input--useAddress')]//button")
	private WebElement botonIngresarDireccion;
	
	@FindBy (xpath="//*[contains(@class, 'fbra_falabellaComponentCheckoutDeliveryActions')]//button")
	private WebElement botonContinuarIrAPagar;
	
	@FindBy (xpath="//*[contains(@class, 'fbra_payment-options-redesign')][4]")
	private WebElement botonPagoDebitoEfectivo;
	
	@FindBy (xpath="//button[text()='CONTINUAR CON PAGO EN EFECTIVO']")
	private WebElement botonContinuarPagoEfectivo;
	
	@FindBy (xpath="//input[@id='firstName']")
	private WebElement inputNombre;

	@FindBy (xpath="//input[@id='lastName']")
	private WebElement inputApellido;
		
	@FindBy (xpath="//input[@id='emailAddress']")
	private WebElement inputEMail;
	
	@FindBy (xpath="//input[@id='userIdNumber']")
	private WebElement inputRut;
	
	@FindBy (xpath="//input[@id='phoneNumber']")
	private WebElement inputTelefono;
	
	@FindBy (xpath="//button[text()='Reservar compra']")
	private WebElement botonReservarCompra;
	
	
	public boolean BuscarProducto(String productName) {
		try {	
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(inputBusqueda));
			inputBusqueda.sendKeys(productName);
			inputBusqueda.sendKeys(Keys.ENTER);
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	public int cercanoACero(ArrayList<Integer> arreglo) {
	 
        int curr = 0;
        int near = arreglo.get(0); 
        
        System.out.println("curr: ");
        System.out.println(curr);
        System.out.println("near: ");
        System.out.println(near);
	    
 	   	
        // find the element nearest to zero
        for (int i = 0; i < arreglo.size(); i++){
            System.out.println("dist from " + arreglo.get(i) + " = " + Math.abs(0 - arreglo.get(i)));
            
            arreglo.set(i, Math.abs(0 - arreglo.get(i)));

        }
        
        System.out.println("residuos a cero: ");
	    System.out.println(arreglo);
	    
	    System.out.println("MENOR: ");
	    int menor = arreglo.indexOf(Collections.min(arreglo));
	    System.out.println(menor);
	    return menor;
	}
	
	
	public boolean seleccionarProducto()  {
		
		try {	
		
			System.out.println("Imprimiendo lista de productos");
		
			List<WebElement> resultsList = driver.findElements(By.xpath("//*[contains(@class, 'search-results-4-grid')]"));
			
			ArrayList<Integer> residuos = new ArrayList<Integer>();
			for (WebElement result:resultsList) {
	            System.out.println(result.findElement(By.className("price-0")).getText().replaceAll("\\D+",""));  
	            int price = Integer.parseInt(result.findElement(By.className("price-0")).getText().replaceAll("\\D+",""));
	            residuos.add(10000 - price); 
	            
	        }
			
			System.out.println("Residuos");
			int indiceProductoPrecioCercano = this.cercanoACero(residuos);
			System.out.println(indiceProductoPrecioCercano);
			System.out.println(resultsList.get(indiceProductoPrecioCercano).getText());
			WebElement elementoResultante = resultsList.get(indiceProductoPrecioCercano);
			
			System.out.println("Elemento resultante: ");
			System.out.println(elementoResultante.getText());

			
			
			
			WebElement boton = elementoResultante.findElement(By.xpath(".//div[contains(@class, 'pod-action')]//button"));
			
			System.out.println("--->");
			System.out.println(boton.getAttribute("id"));
			
			((JavascriptExecutor)driver).executeScript("arguments[0].click();", boton);
			
			return true;
			
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	
	public boolean agregarCarrito()  {
		try {
			/*
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.eligeTuopcion = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buttonForCustomers']/button")));
			eligeTuopcion.click();
			*/
			
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(botonEligeTusOpciones));
			botonEligeTusOpciones.click();

			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	

	public boolean seleccionarTalla()  {
		
		try {	
			/*
			WebDriverWait wait = new WebDriverWait(driver, 60);
			this.eligeTalla = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/button")));
			eligeTalla.click();
			*/
			
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(botonPrimeraTalla));
			botonPrimeraTalla.click();
			
			
			wait.until(ExpectedConditions.elementToBeClickable(botonAgregarABolsa));
			botonAgregarABolsa.click();
			
			
			wait.until(ExpectedConditions.elementToBeClickable(botonIrABolsaDeCompras));
			botonIrABolsaDeCompras.click();
			
			
			wait.until(ExpectedConditions.elementToBeClickable(botonIrAComprar));
			botonIrAComprar.click();
			
			
			return true;
			
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	

	public boolean seleccionarOpcionesDespacho(String region, String comuna) {
		try {		
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOf(selectRegionesElem));
		
			Select selectRegiones = new Select(selectRegionesElem);
			selectRegiones.selectByVisibleText(region);
				
			wait.until(ExpectedConditions.visibilityOf(selectComunaElem));
			Select selectComuna = new Select(selectComunaElem);
			selectComuna.selectByVisibleText(comuna);
			
			wait.until(ExpectedConditions.elementToBeClickable(botonContinuar));
			botonContinuar.click();
			
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	public boolean seleccionarDireccionEntrega(String calle, String numero, String depto) {
		try {
			
			System.out.println("Valores: ");
			System.out.println(calle);
			System.out.println(numero);
			System.out.println(depto);
			
			
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(inputCalle));
			inputCalle.sendKeys(calle);
			
			wait.until(ExpectedConditions.elementToBeClickable(inputNumero));
			inputNumero.sendKeys(numero);
			
			
			wait.until(ExpectedConditions.elementToBeClickable(inputDepto));
			inputDepto.sendKeys(depto);
			
			
			wait.until(ExpectedConditions.elementToBeClickable(botonIngresarDireccion));
			botonIngresarDireccion.click();
			
			
			wait.until(ExpectedConditions.elementToBeClickable(botonContinuarIrAPagar));
			botonContinuarIrAPagar.click();
			
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	public boolean finalizarPago() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(botonPagoDebitoEfectivo));
			botonPagoDebitoEfectivo.click();
			
			wait.until(ExpectedConditions.elementToBeClickable(botonContinuarPagoEfectivo));
			botonContinuarPagoEfectivo.click();
			
			wait.until(ExpectedConditions.elementToBeClickable(inputNombre));
			inputNombre.sendKeys("Rolando Andres");
			
			wait.until(ExpectedConditions.elementToBeClickable(inputApellido));
			inputApellido.sendKeys("Rivero Valladares");
			
			wait.until(ExpectedConditions.elementToBeClickable(inputEMail));
			inputEMail.sendKeys("rolando@gmail.com");
			
			wait.until(ExpectedConditions.elementToBeClickable(inputRut));
			inputRut.sendKeys("267036624");
			
			wait.until(ExpectedConditions.elementToBeClickable(inputTelefono));
			inputTelefono.sendKeys("87321829");

			wait.until(ExpectedConditions.elementToBeClickable(botonReservarCompra));
			botonReservarCompra.click();
			
			
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
