package com.yapo.entities;

import org.testng.annotations.Test;

public class DatosBuscar {

	String textBoxLocator;
	String filtroRegionLocator;
	
	
	
	public void setSelTextBoxLocator(String textBoxLocator) {
		this.textBoxLocator = textBoxLocator;
	}
	public String getSelTextBoxLocator() {
		return textBoxLocator;
	}
	
	public void setSelFiltro1(String filtroRegionLocator) {
		this.filtroRegionLocator = filtroRegionLocator;
	}
	public String getSelFiltro1() {
		return filtroRegionLocator;
	}
}
