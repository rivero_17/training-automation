package com.yapo.entities;
// import org.testng.annotations.Test;

public class DatosPublicar {

	String categorias;
	String condicion;
	String genero;
	String talla;
	String titulo;
	String descripcion;
	String precio;
	String region;
	String comuna;
	String nombre;
	String email;
	String emailConfirm;
	String telefono;
	String contrasena;
	String confirmaContrasena;
	
	
	public void setSelCategoria(String categorias) {
		this.categorias = categorias;
	}
	public String getSelCategoria() {
		return categorias;
	}

	public void setSelCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getSelCondicion() {
		return condicion;
	}
	
	public void setSelGenero(String genero) {
		this.genero = genero;
	}
	public String getSelGenero() {
		return genero;
	}
	public void setSelTalla(String talla) {
		this.talla = talla;
	}
	
	public String getSelTalla() {
		return talla;
	}
	public void setSelTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getSelTitulo() {
		return titulo;
	}
	public void setSelDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getSelDescripcion() {
		return descripcion;
	}
	public void setSelPrecio(String precio) {
		this.precio = precio;
	}
	
	public String getSelPrecio() {
		return precio;
	}
	public void setSelRegion(String region) {
		this.region = region;
	}
	
	public String getSelRegion() {
		return region;
	}
	public void setSelNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getSelNombre() {
		return nombre;
	}
	public void setSelComuna(String comuna) {
		this.comuna = comuna;
	}
	
	public String getSelComuna() {
		return comuna;
	}
	public void setSelEmail(String email) {
		this.email = email;
	}
	
	public String getSelEmail() {
		return email;
	}
	public void setSelEmailConfirm(String emailConfirm) {
		this.emailConfirm = emailConfirm;
	}
	
	public String getSelEmailConfirm() {
		return emailConfirm;
	}
	public void setSelTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getSelTelefono() {
		return telefono;
	}
	public void setSelContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
	public String getSelContrasena() {
		return contrasena;
	}
	public void setSelContraseñaConfirm(String confirmaContrasena) {
		this.confirmaContrasena = confirmaContrasena;
	}
	
	public String getSelContrasenaConfirm() {
		return confirmaContrasena;
	}

}
