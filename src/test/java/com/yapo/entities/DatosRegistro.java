package com.yapo.entities;

public class DatosRegistro {
	String nombreCompleto;
	String telefono;
	String correo;
	String password;
	String regiones;
	String comunas;
	String password2;

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setRegiones(String regiones) {
		this.regiones = regiones;
	}
	
	public String getRegiones() {
		return regiones;
	}
	
	public void setComunas(String comunas) {
		this.comunas = comunas;
	}
	
	public String getComunas() {
		return comunas;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	public String getPassword2() {
		return password2;
	}
	
}
