package com.yapo.test;
import org.testng.annotations.Test;
import com.yapo.config.BaseConfig;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNG_RegistroYapo extends BaseConfig {
	
	/*WebDriver driver;
	By iniSesionLocator =By.id("login-account-link");
	By creAquiLocator= By.xpath("/html/body/div[9]/div/div/div[2]/form/p[2]/strong/a");
	By nameLocator=By.id("account_name");
	By phoneLocator=By.id("phone");
	By emailLocator=By.id("account_email");
	By passwordLocator=By.id("account_password");
	By verifyPassLocator=By.id("account_password_verify");
	By personLocator= By.cssSelector("#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div");
	By generoLocator=By.cssSelector("div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper");
	By regionLocator=By.id("account_region");
	By comunaLocator=By.id("account_commune");
	By terminoLocator=By.id("accept_conditions");
	By RegisterLocator=By.id("edit_profile_btn");*/
	
   
  @Test
  public void Registro_yapo() throws InterruptedException {
	
		
		//llenando el formulario
		//clase para registrarse 
				driver.findElement(By.id("login-account-link")).click();
				Thread.sleep(6000);
				driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/form/p[2]/strong/a")).click();
				
				//llenando el formulario
				driver.findElement(By.id("account_name")).sendKeys("Rolando Rivero");//Nombre completo
				driver.findElement(By.id("phone")).sendKeys("987321829");//Telefono
				driver.findElement(By.id("account_email")).sendKeys("rrivero1@clmconsultores.com");//correo
				driver.findElement(By.id("account_password")).sendKeys("12345678");//password
				driver.findElement(By.id("account_password_verify")).sendKeys("12345678");
				WebElement persona = driver.findElement(By.cssSelector("#account_create_form > fieldset > div.formContainer.field-success > div:nth-child(4) > div > div:nth-child(1) > div"));
			    persona.click();
			    WebElement genero=driver.findElement(By.cssSelector("div.grid-wrapper:nth-child(1) div.create-account-main div.form-wrap.account-create-wrap.formAccount div.create-account-form div.mainForm form.form-account.form-horizontal.form div.formContainer.field-success div.control-group.formItem.formItem__stacked:nth-child(8) div.controls.formItem__input-field.field-success div.formItem__radioButton:nth-child(1) div.iradio_minimal-blue > ins.iCheck-helper"));
			    genero.click();
			    Select region=new Select (driver.findElement(By.id("account_region")));
			    region.selectByVisibleText("Región Metropolitana");
			    WebElement comuna=driver.findElement(By.id("account_commune"));
			    comuna.sendKeys("Huechuraba");
			    WebElement terminos=driver.findElement(By.id("accept_conditions"));
				Actions actions = new Actions(driver);
				actions.moveToElement(terminos).click().perform();
				//driver.findElement(By.id("edit_profile_btn")).click();// boton crear cuenta
	  
  }


  

}