package com.yapo.test;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.yapo.config.BaseConfig;
import com.yapo.dataprovider.RegisterDataProvider;
import com.yapo.entities.DatosPublicar;
// import com.yapo.entities.DatosRegistro;
import com.yapo.page.PublicarAvisoPage;
// import com.yapo.page.RegisterPage;

public class PublicarTest extends BaseConfig {
	@Test  (description = "Publicar en Yapo", dataProvider = "publicar",dataProviderClass = RegisterDataProvider.class)
	public void test(DatosPublicar datosPublicar) {
		
		System.out.println("7585: ");
		System.out.println(datosPublicar.getSelCategoria());
		PublicarAvisoPage PAP= new PublicarAvisoPage(driver);
		SoftAssert soft = new SoftAssert();
	
		soft.assertTrue(PAP.PublicarAviso(datosPublicar.getSelCategoria()), "No se publico el aviso");
		soft.assertTrue(PAP.PublicarCondicion(datosPublicar.getSelCondicion()), "No se selecciono la condición");
		soft.assertTrue(PAP.PublicarGenero(datosPublicar.getSelGenero()), "No se selecciono el genero");
		soft.assertTrue(PAP.PublicarTalla(datosPublicar.getSelTalla()), "No se selecciono la talla");
		soft.assertTrue(PAP.PublicarTitulo(datosPublicar.getSelTitulo()), "No se ingreso el titulo");
		soft.assertTrue(PAP.PublicarDescripcion(datosPublicar.getSelDescripcion()), "No se ingreso descripcion");
		soft.assertTrue(PAP.PublicarPrecio(datosPublicar.getSelPrecio()), "No se ingreso precio");
		soft.assertTrue(PAP.PublicarRegion(datosPublicar.getSelRegion()), "No se encontro la region");
		soft.assertTrue(PAP.PublicarComuna(datosPublicar.getSelComuna()), "No se encontro la comuna");
		soft.assertTrue(PAP.PublicarNombre(datosPublicar.getSelNombre()), "No se ingreso el nombre");
		soft.assertTrue(PAP.PublicarEmail(datosPublicar.getSelEmail()), "No se ingreso el email");
		soft.assertTrue(PAP.PublicarEmailConfirm(datosPublicar.getSelEmailConfirm()), "No se ingreso el email confirmado");
		soft.assertTrue(PAP.PublicarTelefono(datosPublicar.getSelTelefono()), "No se ingreso el fono");
		soft.assertTrue(PAP.PublicarContrasena(datosPublicar.getSelContrasena()), "No se ingreso la contrasena");
		soft.assertTrue(PAP.PublicarContrasenaConfirm(datosPublicar.getSelContrasenaConfirm()), "No se ingreso el fono");
		soft.assertAll();
		
	
		//fail("Not yet implemented");
	}

}

