package com.yapo.test;


import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.yapo.config.BaseConfig;
import com.yapo.dataprovider.RegisterDataProvider;
import com.yapo.entities.DatosRegistro;
import com.yapo.page.BuscarAvisoPage;
import com.yapo.page.PublicarAvisoPage;
import com.yapo.page.RegisterPage;


class RegisterTest extends BaseConfig{
	
	@Test  (description = "Registro de usuario en Yapo", dataProvider = "registro",dataProviderClass = RegisterDataProvider.class)
	public void test(DatosRegistro datosRegistro) {
		
		System.out.println("7585: ");
		System.out.println(datosRegistro.getTelefono());
		
		System.out.println("7586: ");
		System.out.println(datosRegistro.getRegiones());
		
		RegisterPage RP=new RegisterPage(driver);
		// BuscarAvisoPage BA= new BuscarAvisoPage(driver);
		// PublicarAvisoPage PAP= new PublicarAvisoPage(driver);
		SoftAssert soft = new SoftAssert();
		soft.assertTrue(RP.RegisterUser(datosRegistro.getRegiones()), "No se ejecuto el flujo Register user");
		soft.assertTrue(RP.RegisterComuna(datosRegistro.getComunas()), "no se ejecuto el flujo comuna");
		soft.assertTrue(RP.RegisterName(datosRegistro.getNombreCompleto()), "no se ejecuto el flujo comuna");
		soft.assertTrue(RP.RegisterTelefono(datosRegistro.getTelefono()), "no se ejecuto el flujo comuna");
		soft.assertTrue(RP.RegisterPassword(datosRegistro.getPassword()), "no se ejecuto el flujo comuna");
		soft.assertTrue(RP.RegisterCorreo(datosRegistro.getCorreo()), "no se ejecuto el flujo comuna");
		soft.assertTrue(RP.RegisterconfirmarPassword(datosRegistro.getPassword2()), "no se ejecuto el flujo comuna");
		
		//soft.assertEquals(RP.RegisterconfirmarPassword(datosRegistro.getPassword()), RP.RegisterconfirmarPassword(datosRegistro.getPassword2()));
		
			

	
		soft.assertAll();
		
	
		//fail("Not yet implemented");
	}

}
