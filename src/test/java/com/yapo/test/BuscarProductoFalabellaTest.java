package com.yapo.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.yapo.config.BaseConfig;
import com.yapo.dataprovider.BuscarProductoDataProvider;
// import com.yapo.dataprovider.RegisterDataProvider;
import com.yapo.entities.DatosFalabella;
import com.yapo.page.BuscarProductoPage;

public class BuscarProductoFalabellaTest extends BaseConfig {


	
		@Test  (description = "Busqueda de productos en site falabella", dataProvider = "buscarProducto", dataProviderClass = BuscarProductoDataProvider.class)
		public void test(DatosFalabella datosFalabella) {
						
			BuscarProductoPage BPP= new BuscarProductoPage(driver);
			SoftAssert soft = new SoftAssert();
			soft.assertTrue(BPP.BuscarProducto(datosFalabella.getProducts()), "No se ejecuto el flujo Buscar producto");
			soft.assertTrue(BPP.seleccionarProducto(), "No se ejecuto el flujo Buscar producto");
			soft.assertTrue(BPP.agregarCarrito(), "No se ejecuto el flujo para agregar al carrito de compras");
			soft.assertTrue(BPP.seleccionarTalla(), "No se ejecuto el flujo para seleccionar talla");
			soft.assertTrue(BPP.seleccionarOpcionesDespacho(datosFalabella.getRegion(), datosFalabella.getComuna()), "No se ejecuto el flujo para escribir las opciones de despacho");
			soft.assertTrue(BPP.seleccionarDireccionEntrega(
					datosFalabella.getCalle(),
					datosFalabella.getNumero(),
					datosFalabella.getDepto()
			), "No se ejecuto el flujo para escribir las opciones de Direccion de Entrega");
			soft.assertTrue(BPP.finalizarPago(), "No se ejecuto el flujo de pago con efectivo");
			soft.assertAll();

		}

	}




